GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

echo -e "${GREEN}##### Install MySQL #####${NC}"
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password qwerty123'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password qwerty123'
sudo apt-get -y install mysql-server-5.7

#It allows to remote access to database
sudo sed -i 's/= 127.0.0.1/= 0.0.0.0/g' /etc/mysql/mysql.conf.d/mysqld.cnf