GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

echo -e "${GREEN}##### Install PHP 7.2 extensions for Composer #####${NC}"

#Extensions required by composer
sudo apt-get install php7.2-curl -y
sudo apt-get install php7.2-cli -y
sudo apt-get install php7.2-mbstring -y