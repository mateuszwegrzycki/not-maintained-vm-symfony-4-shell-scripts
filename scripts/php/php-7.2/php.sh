GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

echo -e "${GREEN}##### Install PHP #####${NC}"
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install -y php7.2
echo -e "set displaying PHP errors in browser"
sudo sed -i 's/display_errors = Off/display_errors  = On/g' /etc/php/7.2/apache2/php.ini