GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

echo -e "${GREEN}##### Install Apache #####${NC}"
sudo apt-get update
sudo apt-get install apache2 -y
sudo ufw allow 'Apache'
sudo a2enmod rewrite
sudo sed -i "182i <Directory /var/www/html/>\n\tAllowOverride All\n\tRequire all granted\n</Directory> " /etc/apache2/apache2.conf