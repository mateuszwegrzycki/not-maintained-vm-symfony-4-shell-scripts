GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

echo -e "${GREEN}##### Install Symfony #####${NC}"

#Install Symfony binary
sudo wget https://get.symfony.com/cli/installer -O - | bash

#Allow to use 'symfony' command globally
mv /root/.symfony/bin/symfony /usr/local/bin/symfony

#Create Symfony application
#Choose one option from available below:

#1. To create new Symfony project

#Set git settings - it is required to create symfony project via 'symfony' command
##git config --global user.email "you@example.com"
##git config --global user.name "Your Name"

#Remove html directory, because 'symfony' command need to create own directory (If directory exists before setting up symfony project - it is deteced as not empty directory)
##sudo rm -rf /var/www/symfony.local/html
##sudo chown -R vagrant:vagrant /var/www/symfony.local
##sudo chmod -R 755 /var/www/symfony.local

#Create symfony project
##symfony new --full /var/www/symfony.local/html/

#END OF OPTION 1.

# 2. To use existing Symfony project from git repository (specify username and password for repository, but remeber to do not commit them!)
###sudo mkdir /var/www/tmp/
###sudo chmod 777 -R /var/www/tmp/
git clone https://<username>:<password>@bitbucket.org/mateuszwegrzycki/symfony-sandbox.git /var/www/symfony.local/html
###shopt -s dotglob
###sudo mv /var/www/tmp/backend/*  /var/www/symfony.local/html/
###shopt -u dotglob
###sudo rm -rf /var/www/tmp
cd /var/www/symfony.local/html/
composer install
#END OF OPTION 2.

sudo chmod 775 /var/www
find /var/www/ -type d -exec sudo chmod 775 {} \;
find /var/www/ -type f -exec sudo chmod 664 {} \;

sudo usermod -a -G www-data vagrant
sudo chown -R vagrant:www-data /var/www/symfony.local/html/

sudo chmod 777 -R /var/www/symfony.local/html/var/

#sudo chmod u+x /var/www/symfony.local/html/bin/console

#Change database url in Symfony .env
sudo sed -i 's|DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name|DATABASE_URL=mysql://symfony:symfony@127.0.0.1:3306/symfony|g' /var/www/symfony.local/html/.env