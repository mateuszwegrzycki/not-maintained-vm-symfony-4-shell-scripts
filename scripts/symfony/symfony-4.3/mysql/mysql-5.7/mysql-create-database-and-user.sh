GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

echo -e "${GREEN}##### Create user and database for Symfony in MySQL #####${NC}"
mysql -u root -pqwerty123 -e "CREATE USER 'symfony'@'%' IDENTIFIED BY 'symfony'"
mysql -u root -pqwerty123 -e "GRANT ALL PRIVILEGES ON *.* TO 'symfony'@'%' WITH GRANT OPTION"
mysql -u root -pqwerty123 -e "FLUSH PRIVILEGES"
mysql -u symfony -psymfony -e "CREATE DATABASE symfony"