GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

echo -e "${GREEN}##### Add virtual host for Symfony #####${NC}"

#Create /var/www/symfony.local directory
sudo mkdir -p /var/www/symfony.local/html
sudo chown -R vagrant:vagrant /var/www/symfony.local/html
sudo chmod -R 755 /var/www/symfony.local

#Add symfony.local in apache available sites
sudo touch /etc/apache2/sites-available/symfony.local.conf
sudo echo -e "<VirtualHost *:80>\n\tServerAdmin admin@example.com\n\tServerName symfony.local\n\tServerAlias www.symfony.local\n\tDocumentRoot /var/www/symfony.local/html/public\n\tErrorLog ${APACHE_LOG_DIR}/error.log\n\tCustomLog ${APACHE_LOG_DIR}/access.log combined\n\n\t<Directory /var/www/symfony.local/html/public>\n\t\tAllowOverride All\n\t\tOrder Allow,Deny\n\t\tAllow from All\n\n\t\tFallbackResource /index.php\n\t</Directory>\n\n\t<Directory /var/www/symfony.local/html/public/bundles>\n\t\tFallbackResource disabled\n\t</Directory>\n</VirtualHost>" > /etc/apache2/sites-available/symfony.local.conf
sudo a2ensite symfony.local.conf
sudo a2dissite 000-default.conf
sudo systemctl restart apache2