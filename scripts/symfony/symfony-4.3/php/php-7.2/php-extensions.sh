GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

echo -e "${GREEN}##### Install PHP extensions #####${NC}"
#Extensions required by Symfony 4
sudo apt-get install php7.2-ctype -y
sudo apt-get install php7.2-iconv -y
sudo apt-get install php7.2-intl -y
sudo apt-get install php7.2-json -y
sudo apt-get install php7.2-simplexml -y
sudo apt-get install php7.2-tokenizer -y
sudo apt-get install php7.2-zip -y
#For Doctrine
sudo apt-get install php7.2-mysql -y
