#Virtual machine for Symfony

Virtual machine created in VirtualBox, shell scripts in Vagrant.

##What does it contain?
- PHP 7.2:
- Apache 2:
- Symfony 4.3
- Composer
- MySQL 5.7

##Requirements
- Vagrant
- VirtualBox
- Command line (e.g. Git bash, Cmder, ConEmu)

##Installation
1. Go to virtual machine main directory
2. Run:```vagrant up```
3. Add ```symfony.local``` to hosts on host computer

##Default configuration:
- VM IP: 192.168.33.102
- Database:
    - Database name: ```symfony```
    - Database user: ```symfony```
    - Database user password: ```symfony```
- System user:
    - username: vagrant
    - password: vagrant

##Tested on:
- VirtualBox 6.0.10 r132072, Vagrant 2.2.5

##TODO
- Git installation
- XDebug installation